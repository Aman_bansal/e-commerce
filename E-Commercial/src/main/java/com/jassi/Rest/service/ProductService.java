package com.jassi.Rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jassi.Rest.Dao.ProductDao;
import com.jassi.Rest.entity.ProductsApp;

@Service
public class ProductService {

	@Autowired
	ProductDao dao;
	
	public List<ProductsApp> getProducts() {
		return dao.getProducts();
	}

}
