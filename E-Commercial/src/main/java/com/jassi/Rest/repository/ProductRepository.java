package com.jassi.Rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jassi.Rest.entity.ProductsApp;

public interface ProductRepository extends JpaRepository<ProductsApp, Long> {

}
