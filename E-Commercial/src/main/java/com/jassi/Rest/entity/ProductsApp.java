package com.jassi.Rest.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class ProductsApp {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(targetEntity = ProductCategoryApp.class)
	@JoinColumn(name = "category_id")
	private ProductCategoryApp category;
	
	@Column(name = "sku")
	private  String sku;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "image_url")
	private String  imageUrl;
	
	@Column(name = "active")
	private boolean active;
	
	@Column(name = "units_in_stock")
	private int unitInStock;
	
	@Column(name ="unit_price" )
	private BigDecimal unitInPrice;
	
	@Column(name = "date_created")
	@CreationTimestamp
	private Date dateCreated;

	@Override
	public String toString() {
		return "Product [id=" + id + ", category=" + category + ", sku=" + sku + ", name=" + name + ", description="
				+ description + ", imageUrl=" + imageUrl + ", active=" + active + ", unitInStock=" + unitInStock
				+ ", unitInPrice=" + unitInPrice + ", dateCreated=" + dateCreated + "]";
	}
	
}
