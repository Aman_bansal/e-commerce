package com.jassi.Rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jassi.Rest.entity.ProductsApp;
import com.jassi.Rest.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	ProductService service;
	
	@RequestMapping("/products")
	public  List<ProductsApp> getProducts()
	{
		List<ProductsApp> data =service.getProducts();
		System.out.println(data);
		return data;
	}

	
}
