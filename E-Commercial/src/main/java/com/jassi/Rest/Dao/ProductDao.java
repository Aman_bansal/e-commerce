package com.jassi.Rest.Dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jassi.Rest.entity.ProductsApp;
import com.jassi.Rest.repository.ProductRepository;

@Repository
public class ProductDao {
	@Autowired
	ProductRepository repo;

	public List<ProductsApp> getProducts() {
		
		List<ProductsApp> products =repo.findAll();
		return products;
	}

}
